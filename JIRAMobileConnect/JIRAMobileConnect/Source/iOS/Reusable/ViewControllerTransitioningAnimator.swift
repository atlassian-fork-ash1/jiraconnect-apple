//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

class ViewControllerTransitioningAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  var animateTransitionContext: UIViewControllerContextTransitioning!
  var participants: ViewControllerTransitioningParticipants!
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
    fatalError("ViewControllerTransitioningAnimator is an abstract class, please implement transitionDuration: in a subclass.".formattedLoggingStatement)
  }
  
  final func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    guard transitionContext.isAnimated() else {
      transitionContext.completeTransition(true)
      return
    }
    
    do {
      animateTransitionContext = transitionContext
      participants = try ViewControllerTransitioningParticipants(transitionContext: transitionContext)
      animateTransition()
    } catch {
      assertionFailure("\(error)".formattedLoggingStatement)
      transitionContext.completeTransition(false)
      return
    }
  }
  
  func animateTransition() {
    fatalError("ViewControllerTransitioningAnimator is an abstract class, please implement animateTransition in a subclass.".formattedLoggingStatement)
  }
  
}

class ViewControllerTransitioningParticipants {
  private(set) var transitionContainerView: UIView!
  private(set) var fromView: UIView!
  private(set) var toView: UIView!
  private(set) var fromViewController: UIViewController!
  private(set) var toViewController: UIViewController!
  private(set) var finalFrameForFromViewController: CGRect!
  private(set) var finalFrameForToViewController: CGRect!
  
  init(transitionContext: UIViewControllerContextTransitioning) throws {
    guard let transitionContainerView = transitionContext.containerView(),
      let fromView = transitionContext.viewForKey(UITransitionContextFromViewKey),
      let toView = transitionContext.viewForKey(UITransitionContextToViewKey),
      let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey),
      let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
      else {
        throw ViewControllerTransitioningError.ExtractParticipantsError
    }
    self.transitionContainerView = transitionContainerView
    self.fromView = fromView
    self.toView = toView
    self.fromViewController = fromViewController
    self.toViewController = toViewController
    self.finalFrameForFromViewController = transitionContext.finalFrameForViewController(fromViewController)
    self.finalFrameForToViewController = transitionContext.finalFrameForViewController(toViewController)
  }
  
  func swapViewControllerViews() {
    toView.frame = finalFrameForToViewController
    transitionContainerView.addSubview(toView)
    toView.layoutIfNeeded() // Layout is needed in order for transition to use destination view's layout for reference.
    fromView.frame = finalFrameForFromViewController
    fromView.removeFromSuperview()
  }
}

enum ViewControllerTransitioningError: String, ErrorType {
  case ExtractParticipantsError = "UIKit failed to provide transition animator instances from transitionContext. Behavior is undefined."
}
