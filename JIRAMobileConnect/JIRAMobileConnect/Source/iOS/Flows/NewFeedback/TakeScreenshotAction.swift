//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

class TakeScreenshotAction: AsyncAction {
  private let application: UIApplication
  private let keyWindow: UIWindow
  private let flashView: UIView
  private(set) var screenshotImage = UIImage()
  
  init?(application: UIApplication) {
    guard let keyWindow = application.keyWindow else { return nil }
    self.application = application
    self.keyWindow = keyWindow
    self.flashView = keyWindow.createScreenshotFlashView()
  }
  
  override func run() {
    keyWindow.addSubview(flashView)
    self.animateScreenshotFlash()
  }
  
  private func animateScreenshotFlash() {
    UIView.animateWithDuration(0.6,
      animations: {
        self.flashView.alpha = 0
      }) { completed in
        self.originalTakeScreenshot()
    }
  }
  
  private func takeScreenshot() {
    let screen = UIScreen.mainScreen()
    let snapshotView = screen.snapshotViewAfterScreenUpdates(true)

    UIGraphicsBeginImageContextWithOptions(snapshotView.bounds.size, true, 0)
    snapshotView.drawViewHierarchyInRect(snapshotView.bounds, afterScreenUpdates: true)
    screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    finishedExecutingOperation()
  }

  // Keeping the original implementation in case the new one above breaks in a new version of iOS.
  private func originalTakeScreenshot() {
    let windowsBackToFront = application.windows.sort { $0.windowLevel < $1.windowLevel }
    let windowImages = windowsBackToFront.map { window in (window, window.takeScreenshot()) }

    UIGraphicsBeginImageContext(UIScreen.mainScreen().bounds.size)
    windowImages.forEach { (window, windowImage) in windowImage.drawInRect(window.frame) }
    screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()

    finishedExecutingOperation()
  }
  
  override  func finishedExecutingOperation()  {
    flashView.removeFromSuperview()
    super.finishedExecutingOperation()
  }
}
