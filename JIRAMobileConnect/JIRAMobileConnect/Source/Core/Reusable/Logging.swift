//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

extension String {
  var formattedLoggingStatement: String {
    return "JIRA Mobile Connect: \(self)"
  }
}