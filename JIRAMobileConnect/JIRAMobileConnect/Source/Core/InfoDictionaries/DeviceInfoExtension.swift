//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

// TODO: Move this file into iOS directory, this extension depends on UIKit.

import Foundation

extension UIDevice {
  var infoAsDictionary: [String: NSObject] {
    return ["devName": name,
      "systemName": systemName,
      "systemVersion": systemVersion,
      "model": model,
      "uuid": identifierForVendorString]
  }
}

extension UIDevice {
  var identifierForVendorString: String {
    return identifierForVendor?.UUIDString ?? ""
  }
}
